# CS 8803: Human and Machine Learning

* [Assignment 1](assignment-1-decision-trees.ipynb)
  (`assignment-1-decision-trees.ipynb`)
* [Assignment 2](assignment-2-prototype-and-exemplar-models.ipynb)
  (`assignment-2-prototype-and-exemplar-models.ipynb`)
* [Assignment 3](assignment-3-vector-representations-and-lsa.ipynb)
  (`assignment-3-vector-representations-and-lsa.ipynb`)
* [Assignment 4](assignment-4-neural-networks.ipynb)
  (`assignment-4-neural-networks.ipynb`)
* [Assignment 5](assignment-5-levels-of-processing.ipynb)
  (`assignment-5-levels-of-processing.ipynb`)
* [Assignment 6](assignment-6-empirical-paper-summary.ipynb)
  (`assignment-6-empirical-paper-summary.ipynb`)
* [Assignment 7](assignment-7-logistic-regression-and-k-nearest-neighbors.ipynb) (`assignment-7-logistic-regression-and-k-nearest-neighbors.ipynb`)
* [Assignment 8](assignment-8-mds-with-vector-representations.ipynb) (`assignment-8-mds-with-vector-representations.ipynb`)

Use either `git clone
https://gitlab.com/vijaymarupudi/cs-8803-hml-2022`, `git pull
https://gitlab.com/vijaymarupudi/cs-8803-hml-2022` or the download
icon on near the right (next to clone icon) to download the files and
access the Jupyter notebooks for the assignments.


