{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fc6606e3",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Assignment 1: Decision Trees\n",
    "\n",
    "**Please do not consult external resources for this assignment.**\n",
    "\n",
    "Decision trees have been well-explored in the field of machine\n",
    "learning. However, we expect you to provide your opinions and ideas on\n",
    "how decision trees can be used to model human learning.\n",
    "\n",
    "Please make sure you read [Mitchell, T. M.\n",
    "(1997)](https://www.cin.ufpe.br/~cavmj/Machine%20-%20Learning%20-%20Tom%20Mitchell.pdf)\n",
    "[chapter 3, pp. 52-60] or [Witten, I. H., et al.\n",
    "(2017)](https://gatech.instructure.com/courses/263714/files?preview=34186721)\n",
    "before starting the assignment. These readings are available on Canvas\n",
    "under Files.\n",
    "\n",
    "In this assignment, we are going to use the [ID3\n",
    "Algorithm](https://en.wikipedia.org/wiki/ID3_algorithm) to induce a\n",
    "decision tree from the classic dataset in [Quinlan,\n",
    "1986](https://link.springer.com/content/pdf/10.1007%2FBF00116251.pdf).\n",
    "\n",
    "Please download quinlan-1986.csv (found in the files directory) and\n",
    "store it in your local directory.\n",
    "\n",
    "![](static/quinlan-1986-dataset.png)\n",
    "\n",
    "**Some of the code will be filled in for you, and in other places\n",
    "pseudo code will be provided. As a part of this assignment, please\n",
    "fill in the pseudocode. At the end of this notebook, you'll have a\n",
    "generated decision tree. In addition, answer the questions listed at\n",
    "the end of the assignment**\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5a58da0-6a70-4d08-9ca1-5b6c437ea30c",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Programming notes\n",
    "\n",
    "This assignment will require the use of Python, and the numpy and\n",
    "pandas libraries. Please refer to the following tutorials if you are\n",
    "not familiar with them.\n",
    "\n",
    "* [Python, if you know another programming language](https://learnxinyminutes.com/docs/python/)\n",
    "* [Python, if you are new to programming](https://www.codecademy.com/learn/learn-python-3)\n",
    "* [Numpy](https://scipy-lectures.org/intro/numpy/index.html)\n",
    "* [Pandas](https://pandas.pydata.org/docs/getting_started/intro_tutorials/index.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "541e575b",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Dependencies and library.\n",
    "\n",
    "Please ensure that you have `pandas` and `numpy` installed. If not,\n",
    "please run the following lines to install them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37ed244b",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!pip3 install numpy\n",
    "!pip3 install pandas"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "836c4e78-9fbb-47f4-b674-9f7e6dddf383",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's import the modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49a104c2",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35e794fd",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Loading & Preprocessing\n",
    "\n",
    "Next, we load the dataset and separate the class (outcome) data from\n",
    "the rest of the dataset for implementation purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "725a2d6b",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "d = pd.read_csv(\"quinlan-1986.csv\")\n",
    "class_ = d[\"class\"]\n",
    "training_data = d.drop(\"class\", axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc3bfd36",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## ID3 Algorithm Functions\n",
    "\n",
    "ID3 needs to know if splitting a decision tree perfectly partitions\n",
    "the input. For that reason, we define a function to check whether a\n",
    "series of class values has only one unique value.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fffdaa29-f2c2-43bd-9042-36fb79c605b0",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def is_pure(class_series):\n",
    "    return len(np.unique(class_series)) == 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "528e8f4f-0ed1-4ae1-862a-21c9af35c835",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "ID3 utilizes the metric of information gain to determine the best\n",
    "features to utilize for split the data at each node of the decision\n",
    "tree. Information gain is calculated using entropy, in which entropy\n",
    "is a measure of impurity and nonhomogeneity in the dataset. For\n",
    "example, entropy is 0 if all datapoints in a dataset belong to a\n",
    "single class.\n",
    "\n",
    "**Please complete the `entropy()` and `information_gain()` functions\n",
    "using the following equations.** Do not worry about computational\n",
    "efficiency, just make sure you get the logic right.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05fd07fa-8f1d-4cb1-985b-e610f86e1227",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Entropy\n",
    "\n",
    "The following equation describes how to calculate entropy for a dataset with\n",
    "$c$ classes.\n",
    "\n",
    "$$E(S) = \\sum_{i=1}^{c} -p_{i}\\log_{2}p_{i}$$\n",
    "\n",
    "Utilize the above equation to implement the `entropy()` function. The\n",
    "function takes in as input `sample_counts`, which is an array\n",
    "containing the total number of samples associated with each class. \n",
    "\n",
    "For example, if there are 4 Ns and 5 Ps, then `sample_count` would be `[4, 5]`.\n",
    "\n",
    "The function should return a number. Note that log(0) is not\n",
    "mathematically defined, use the value 0 in this case.\n",
    "\n",
    "**Test cases**\n",
    "\n",
    "* `entropy(np.array([1, 0, 0, 0]))` == 0\n",
    "* `entropy(np.array([1, 1, 0, 0]))` == 1\n",
    "* `entropy(np.array([1, 1, 0, 10]))` ~ 0.8167\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "852e348c",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def entropy(sample_counts):\n",
    "    ### Implement entropy() and remove the 'pass' to run your code\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94023f2a",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Information Gain\n",
    "\n",
    "`information_gain()` takes in as input a DataFrame with `cases` (*S*), a\n",
    "vector with `classes` for the cases, and the `column_name` (*A*) that\n",
    "we would like to determine the *gain* of splitting on:\n",
    "\n",
    "Calculate the information gain using the following equation:\n",
    "\n",
    "$$\\text{Gain}(S,A) = \\text{Entropy}(S) - \\sum_{v\\in \\text{Values}(A)} \\frac{|S_{v}|}{|S|} \\text{Entropy}(S_{v})$$\n",
    "\n",
    "where\n",
    "\n",
    "* $\\text{Entropy}(S)$ calculates the entropy across the entire sample S,\n",
    "* $\\text{Values}(A)$ is the set of all possible values for an\n",
    "  attribute $A$, and\n",
    "* $S_{v}$ is the subset of $S$ for which attribute $A$ has value $v$.\n",
    "\n",
    "You may find these functions useful:\n",
    "\n",
    "* [`Series.value_counts()`](https://pandas.pydata.org/docs/reference/api/pandas.Series.value_counts.html)\n",
    "* [`pd.crosstab()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.crosstab.html)\n",
    "\n",
    "The function should return the gain value (number) for splitting `cases` on\n",
    "`column_name`, using the `entropy()` function you defined earlier.\n",
    "\n",
    "**Test cases**\n",
    "\n",
    "The first four splits for these columns should have these values (from the slides)\n",
    "\n",
    "* outlook == `0.24674981977443933`\n",
    "* temperature == `0.02922256565895487`\n",
    "* humidity == `0.15183550136234159`\n",
    "* windy == `0.04812703040826949`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "815cd7d2",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def information_gain(cases, classes, column_name):\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7658687",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## ID3 Training \n",
    "\n",
    "Finally, we require a training loop that keeps splitting on variables (features) in the dataset, sorted by information gain, until we have a perfect tree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "563431b6",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def train_id3(node, X, y):\n",
    "    # if the dataset is perfectly separated, then stop, add a leaf node\n",
    "    if is_pure(y):\n",
    "        node_name = y.iloc[0] # the outcome value\n",
    "        node[\"children\"].append(node_name)\n",
    "    else:\n",
    "\n",
    "        # calculate information gain for splitting on each variable.\n",
    "        igs = []\n",
    "        for col in X.columns:\n",
    "            igs.append(information_gain(X, y, col))\n",
    "        # get the feature with the max information gain\n",
    "        max_index = np.argmax(igs)\n",
    "        feature_name = X.columns[max_index]\n",
    "\n",
    "        # split the dataset that feature. Now add a child node for each\n",
    "        # variable of that feature, and recursively complete the\n",
    "        # process.\n",
    "        grouped = X.groupby(feature_name)\n",
    "        children_nodes = []\n",
    "        for key, subset_X in grouped:\n",
    "            child_node = dict(name=f\"{feature_name}={key}\", children=[])\n",
    "            node[\"children\"].append(child_node)\n",
    "            subset_y = y[X[feature_name] == key]\n",
    "            train_id3(child_node, subset_X, subset_y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1cb7cb7a",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Generating the decision tree\n",
    "\n",
    "The code is structured so that a Node is a dictionary with a name and\n",
    "a list of children. Let's induce the decision tree.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "330fe640",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "node = dict(name=\"root\", children=[])\n",
    "train_id3(node, training_data, class_)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4be2f30",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Visualizing the decision tree\n",
    "\n",
    "The node dictionary is populated with each node's children provided as\n",
    "a list.\n",
    "\n",
    "We can now visualize the decision tree using two ways. The first way\n",
    "is to pretty print the dictionary. Another way is to use the `graphviz`\n",
    "package and generate the DOT source for it to graph."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff116a8c",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Visualize using pretty print\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "066ee88f",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pprint import pp\n",
    "pp(node)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad79f754",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "### Visualize using graphviz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8b86fe9d",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import random # for generating node ids\n",
    "import graphviz\n",
    "\n",
    "def make_dot_internal(node, lines, i):\n",
    "    if isinstance(node, str):\n",
    "        lines.append(f'{i} [label=\"{node}\"];')\n",
    "    if isinstance(node, dict):\n",
    "        lines.append(f'{i} [label =\"{node[\"name\"]}\"];')\n",
    "        for j, child in enumerate(node[\"children\"]):\n",
    "            child_num = random.randint(0, 100000000)\n",
    "            make_dot_internal(child, lines, child_num)\n",
    "            lines.append(f'{i} -> {child_num};')\n",
    "    \n",
    "def make_dot(node):\n",
    "    lines = []\n",
    "    make_dot_internal(node, lines, 0)\n",
    "    node_data = \"\\n\".join(lines)\n",
    "    return f\"digraph Tree {{ node[shape=box];\\n{node_data} }}\"\n",
    "\n",
    "graphviz.Source(make_dot(node))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "729734a7",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Questions\n",
    "\n",
    "Respond to each question in one or two short paragraphs. Remember, we\n",
    "are looking for connections to psychological plausibility in humans."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1825e16",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "1. What does your decision tree look like? Please include the code\n",
    "  you used to induce the tree, and a visualization (text or image) of\n",
    "  the tree.\n",
    "\n",
    "2. Comment on the ID3 algorithm for inducing decision trees. Is the\n",
    "  algorithm psychologically plausible? Which parts of the algorithm do\n",
    "  you think are psychologically plausible, and which parts are not?\n",
    "  What limitations do you see in decision trees? What kind of\n",
    "  concepts can or can't they learn? Do you see the same limitations in\n",
    "  humans?\n",
    "\n",
    "3. What advantages do you see in decision trees? Do you see the same\n",
    "  behavior in humans?\n",
    "\n",
    "4. Note that the dataset above was perfectly separable. How would you\n",
    "  expand on the ID3 algorithm, or decision trees in general, to handle\n",
    "  noise in the data? Would this extension make decision trees a more\n",
    "  feasible model of human cognition? Why or why not?\n",
    "\n",
    "5. Note that the dataset above only contained categorical values. How\n",
    "  would you expand the ID3 algorithm, or decision trees in general, to\n",
    "  handle continuous values in the data? Would this extension make\n",
    "  decision trees a more feasible model of human cognition? Why or why\n",
    "  not?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19bc9955",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Credits\n",
    "\n",
    "Equations are referenced from Chapter 3: Decision Tree Learning from\n",
    "Mitchell, T. M. (1997).\n",
    "\n",
    "## Submission Guidelines\n",
    "\n",
    "Please submit this Jupyter notebook including your code and your\n",
    "answers to the questions to Assignment 1 on Canvas.\n",
    "\n",
    "The code should not depend on any system specific configuration."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "argv": [
    "python",
    "-m",
    "ipykernel_launcher",
    "-f",
    "{connection_file}"
   ],
   "display_name": "Python 3 (ipykernel)",
   "env": null,
   "interrupt_mode": "signal",
   "language": "python",
   "metadata": {
    "debugger": true
   },
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  },
  "name": "Assignment_1-_Decision_Trees.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
