{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4f914f7c-cc8d-4c21-ad0f-a4828529fad3",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Assignment 8: Multidimensional Scaling with Vector Representations\n",
    "\n",
    "Make sure that you have read the required readings for this homework:\n",
    " \n",
    "\n",
    "* [Sebastian Ruder, On word embeddings – part 1: [word2vec]](https://ruder.io/word-embeddings-1/)\n",
    "* [Sebastian Ruder, On word embeddings – part 3:  [GloVe]](https://ruder.io/secret-word2vec/)\n",
    "\n",
    "or \n",
    "\n",
    "* [Jay Alammar, The Illustrated Word2Vec](https://jalammar.github.io/illustrated-word2vec/)\n",
    "\n",
    "\n",
    "In this assignment, we are going to conduct an analysis of word\n",
    "embeddings from GloVe by comparing the distances and relationships\n",
    "between the various embeddings with data from human subjects. This\n",
    "homework will build on the content taught in lecture, so please review\n",
    "the slides for this lecture before starting this assignment.\n",
    "\n",
    "Multidimentional scaling typically requires a distance matrix as\n",
    "input, and allows the user to fix the number of dimensions to reduce\n",
    "the data to while still trying to maintain the pairwise distances\n",
    "between the observations.\n",
    "\n",
    "In our case, our data from humans came in the form of a distance\n",
    "matrix, as participants were shown pairs of words in various\n",
    "categories and were asked to rate how similar all pairs of words in a\n",
    "category were to each other.\n",
    "\n",
    "## Loading the data\n",
    "\n",
    "To make it easier for you, we've included the human data directly in\n",
    "the code. Please make sure you understand how the words correspond to\n",
    "the numbers in the matrices.\n",
    "\n",
    "For example, take the anger category. The words for the *Anger* category\n",
    "are \"calm\", \"annoyed\", \"angry\", and \"furious\". The distance matrix\n",
    "then is a 4x4 matrix, with each cell representing the distance between\n",
    "the two words. Both the rows and columns correspond to the words in\n",
    "order, i.e., element 2, 3 corresponds to the distance between\n",
    "\"annoyed\" and \"angry\" and element 4, 1 corresponds to the distance\n",
    "between \"furious\" and \"calm\". Note that the distance between a word\n",
    "and itself is zero, and the distance between word *x* and word *y* is the\n",
    "same as the distance between word *y* and word *x*, resulting in a\n",
    "symmetric matrix with a zero diagonal.\n",
    "\n",
    "Now we're ready to start coding. Let's import the libraries first.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fe37aa18-c9f8-4604-9c25-8c6ee0cef59f",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.manifold import MDS\n",
    "import numpy as np\n",
    "from scipy.spatial.distance import cosine\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c58e6927-d919-4267-8f13-2cd22a10be47",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now let's load the data! Here's the human data from Varma et al. (2022). People estimated the similarity between all the unique pairs of words for all 5 categories. We converted the similarity ratings into distance matrices for you.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa4c36c6-5b97-44a3-b3fb-24446cb9d7a9",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "human_distance_matrix_by_category = {\n",
    "  'anger': np.array(\n",
    "      [[0.  , 0.74, 0.81, 0.83],\n",
    "       [0.74, 0.  , 0.39, 0.45],\n",
    "       [0.81, 0.39, 0.  , 0.2 ],\n",
    "       [0.83, 0.45, 0.2 , 0.  ]]),\n",
    " 'danger': np.array(\n",
    "     [[0.  , 0.8 , 0.84, 0.87],\n",
    "      [0.8 , 0.  , 0.34, 0.48],\n",
    "      [0.84, 0.34, 0.  , 0.25],\n",
    "      [0.87, 0.48, 0.25, 0.  ]]),\n",
    " 'greatness': np.array(\n",
    "     [[0.  , 0.38, 0.49, 0.52],\n",
    "      [0.38, 0.  , 0.3 , 0.35],\n",
    "      [0.49, 0.3 , 0.  , 0.24],\n",
    "      [0.52, 0.35, 0.24, 0.  ]]),\n",
    " 'probability': np.array(\n",
    "     [[0.  , 0.66, 0.72, 0.78],\n",
    "      [0.66, 0.  , 0.36, 0.48],\n",
    "      [0.72, 0.36, 0.  , 0.43],\n",
    "      [0.78, 0.48, 0.43, 0.  ]]),\n",
    " 'quantity': np.array(\n",
    "     [[0.  , 0.72, 0.85, 0.83],\n",
    "      [0.72, 0.  , 0.57, 0.62],\n",
    "      [0.85, 0.57, 0.  , 0.28],\n",
    "      [0.83, 0.62, 0.28, 0.  ]])}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3aaac97-ee71-4bc5-ae4d-55702361801f",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's enumerate the words and their categories to use when running MDS and\n",
    "visualizing its results.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66d34df0-adac-4125-a4e2-433ef2138729",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "words_by_category = {\n",
    "    \"anger\": \"\"\"calm\n",
    "annoyed\n",
    "angry\n",
    "furious\"\"\".split(),\n",
    "    \"danger\": \"\"\"safe\n",
    "sketchy\n",
    "dangerous\n",
    "deadly\"\"\".split(),\n",
    "    \"greatness\" : \"\"\"fair\n",
    "good\n",
    "great\n",
    "best\"\"\".split(),\n",
    "    \"probability\": \"\"\"rare\n",
    "possible\n",
    "probable\n",
    "certain\"\"\".split(),\n",
    "    \"quantity\": \"\"\"none\n",
    "some\n",
    "most\n",
    "all\"\"\".split()\n",
    "}\n",
    "\n",
    "words_of_interest = set()\n",
    "\n",
    "for words in words_by_category.values():\n",
    "    words_of_interest.update(words)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2309bf2-4474-4b5f-8215-3341c4cb0fab",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now, let's load the word vectors from GloVe (Pennington et al., 2014). We're\n",
    "going to use the pretrained vectors the authors provide on their\n",
    "[website](https://nlp.stanford.edu/projects/glove/). Download a `.zip`\n",
    "file of any model from their webpage and extract a text file containing the\n",
    "vectors from it.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a20f557f-c852-4ea7-8cd3-8036118eb4ee",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "word_vectors = {}\n",
    "with open(\"glove.6B.300d.txt\", encoding='utf-8') as f:\n",
    "    for line in f:\n",
    "        word, rest = line.split(\" \", 1)\n",
    "        if word in words_of_interest:\n",
    "            word_vectors[word] = np.array(rest.split()).astype(\"float\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b13ae9ab-88fc-4a98-83b3-15c9fc614a5b",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now use the word embeddings from GloVe to obtain a distance matrix of the word in each category."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "024a8630-f3af-4b83-b0a6-66f2a506f758",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# glove_distance_matrix_by_category should be a dictionary mapping a category to a distance matrix, similar to human_distance_matrix_by_category\n",
    "glove_distance_matrix_by_category = {}\n",
    "\n",
    "# the variable words_by_category is relevant here\n",
    "\n",
    "## TODO: populate glove_distance_matrix_by_category\n",
    "pass"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58dc68ed-368f-4b60-b442-23aa6926d62b",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Great! Now let's run MDS on the distance matrices and visualize the results.\n",
    "\n",
    "\n",
    "Complete the `handle_mds()` function. It should use MDS to convert the\n",
    "distance matrix to a 1-dimensional vector and then graph the\n",
    "results. Refer to the [scikit-learn documentation on\n",
    "MDS](https://scikit-learn.org/stable/modules/generated/sklearn.manifold.MDS.html)\n",
    "for usage instructions. We have already imported the `MDS` class. You'll want the `dissimilarity` keyword\n",
    "argument to be `\"precomputed\"` and `n_components` to be `1`. After\n",
    "fitting, the `model.embedding_` attribute will contain the positions\n",
    "of the words in space. `model.stress_` contains the stress value of\n",
    "the model fit. Use the embedding to graph the words using\n",
    "`matplotlib`. The function `plt.scatter` will be useful here. Label\n",
    "the words and the graphs with the model, category, and stress so that\n",
    "you can analyze them later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "906528a6-cd6b-4e9f-a1ca-a697e10ff047",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def handle_mds(M, words, category, label):\n",
    "    # TODO\n",
    "    pass\n",
    "\n",
    "def handle_dict(d, label):\n",
    "    for category, M in d.items():\n",
    "        handle_mds(M, words_by_category[category], category, label)\n",
    "\n",
    "# example running it on the human distance matrices\n",
    "handle_dict(human_distance_matrix_by_category, \"human\")\n",
    "handle_dict(glove_distance_matrix_by_category, \"GloVe\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f4079bd-7a57-443f-b031-c9a31661e094",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "You should now being seeing some figures that look like this:\n",
    "\n",
    "![](static/example-mds-output.png)\n",
    "\n",
    "\n",
    "Great! It's now analysis time. You may want to view the MDS embeddings\n",
    "directly by printing them for the analysis. Time to answer some questions.\n",
    "\n",
    "## Questions\n",
    "\n",
    "* Submit a visualization of your MDS embeddings for both the human and\n",
    "  GloVe data. There should be 10 images, 5 for the human ratings and 5\n",
    "  for the GloVe word embeddings.\n",
    "* Comment on the MDS solutions generated from the human data. Does the\n",
    "  ordinal arrangement of the adjectives in each set make sense, i.e.,\n",
    "  are they correctly ordered from \"least\" to \"most\"? (Or from \"most\"\n",
    "  to \"least\" -- MDS is agnostic on left-right ordering in the\n",
    "  solutions it generates.) Do the distances between the words follow\n",
    "  the functional form you were expecting? Describe the patterns and\n",
    "  provide hypotheses that might explain them.\n",
    "* Consider the same questions as in (2) for GloVe's MDS solutions.\n",
    "* Now compare the MDS solutions for the human participants and for\n",
    "  GloVe. What differences do you notice? What does the model capture\n",
    "  of human's word representations, and what does it miss? Provide\n",
    "  hypotheses for why it succeeds and why it fails.\n",
    "* What is the **stress** statistic that is outputted for each MDS\n",
    "  solution? What does this statistic represent? What are \"acceptable\"\n",
    "  ranges for it, and what values suggest that more dimensions may be\n",
    "  needed to adequately capture the distances/similarities?\n",
    "* **Choose and complete ONE of the following**:\n",
    "\n",
    "  1. Investigate the dimensionality of the solution. This homework\n",
    "     enforced a one-dimensional solution to match the nature of number\n",
    "     lines. Try to generate two-dimensional solutions for all 5 sets\n",
    "     of human ratings and GloVe word embeddings. Is the extra\n",
    "     dimension informative? If so, try to interpret what the dimension\n",
    "     could mean.\n",
    "\n",
    "  2. Fit a linear function to each of the 1-dimensional solutions (5\n",
    "     for the human ratings and 5 for the GloVe word embeddings). How\n",
    "     linear are human and GloVe representations? Compare them to each\n",
    "     other.\n",
    "     \n",
    "  3. Compare the output from Glove models of differing sizes. Do\n",
    "     larger models do a better job of getting the word order right?\n",
    "     Are they more similar to humans?\n",
    "\n",
    "## References\n",
    "\n",
    "* Pennington, J., Socher, R., & Manning, C.D. (2014). *GloVe: Global\n",
    "  Vectors for Word Representation*.\n",
    "  \n",
    "* Varma, S., Sanford, E., Schaffer, O. A., Marupudi, V., & Lea, R. B.\n",
    "  (in revision). Recruitment of magnitude representations to process\n",
    "  graded words. Cognitive Psychology.\n",
    "\n",
    "## Submission guidelines\n",
    "\n",
    "Please submit your Jupyter notebook to Canvas.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea96135f-41f3-4d58-b693-3dba1d80b25c",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "argv": [
    "python",
    "-m",
    "ipykernel_launcher",
    "-f",
    "{connection_file}"
   ],
   "display_name": "Python 3 (ipykernel)",
   "env": null,
   "interrupt_mode": "signal",
   "language": "python",
   "metadata": {
    "debugger": true
   },
   "name": "python3"
  },
  "name": "assignment-8-mds-with-vector-representations.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
