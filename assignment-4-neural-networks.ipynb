{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "20ee49ce-df3f-45aa-9850-578c444deb25",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Assignment 4: Neural Networks\n",
    "## Part 1: Manual calculation of neural network output and gradient descent \n",
    "\n",
    "The figure below shows a 2-layer, feed-forward neural network with two\n",
    "hidden-layer nodes and one output node. $x_1$ and $x_2$ are the two inputs,\n",
    "and $o_1$ is the output. For the following questions, assume the learning\n",
    "rate is $0.5$.  Each node also has a bias input value ($x_0$) of $1$.\n",
    "Assume there is a sigmoid ($\\sigma(x)$) activation function at the hidden\n",
    "layer nodes and at the output layer node.\n",
    "\n",
    "There are a variety of activation functions, each with different pros and cons.\n",
    "We are using the sigmoid activation function for this assignment. It has the\n",
    "property of clamping the output between 0 and 1. Note that sigmoid function is\n",
    "\n",
    "$$\n",
    "\\sigma(x) = \\frac{1}{1 + e^{-x}}\n",
    "$$\n",
    "\n",
    "and its derivative is\n",
    "\n",
    "$$\n",
    "\\sigma'(x) = \\sigma(x) (1 - \\sigma(x))\n",
    "$$\n",
    "\n",
    "**Note that this is not the update rule. You will find this derivative in the\n",
    "update rule equation in the slides.**\n",
    "\n",
    "![](static/neural_network_manual_calculation.png)\n",
    "\n",
    "Calculate the output values at nodes $h_1$, $h_2$ and $o_1$ of this\n",
    "network for input $x_1 = 1, x_2 = 0$. Remember that bias term $x_0 = 1$.\n",
    "Each unit produces as its output the real value computed by the unit's\n",
    "associated sigmoid function.\n",
    "\n",
    "Then calculate one step of backpropagation, for the same input and target\n",
    "output $t = 1$ for $o_1$. Compute updated weights for the 3 connections\n",
    "into output layer, and then for the 6 connections into the hidden layer. There\n",
    "should be 9 updated weights in total. Make sure you understand when the\n",
    "activation function for a layer is applied in a neural network, and use the\n",
    "derivative of the activation function combined with the learning rate to\n",
    "determine the change in weights.\n",
    "\n",
    "People use many different functions as the loss function, depending on the task\n",
    "and the properties of the output data. For this question, use the L1 loss\n",
    "function to determine the error for backpropagation. L1 is defined as\n",
    "\n",
    "$$\n",
    "  \\text{L1} = \\frac{1}{n} \\sum^n_{i = 1} t_i - o_i\n",
    "$$\n",
    "\n",
    "where n is the number of dimensions in the output layer. It's a simple function\n",
    "calculating the difference between the target vector and the output vector. In\n",
    "this case, the output node is a single real value, so $n = 1$. And the loss\n",
    "becomes\n",
    "\n",
    "$$\n",
    "  \\text{L1} =  t - o_1\n",
    "$$\n",
    "\n",
    "Feel free to refer to the slides, which contain all the equations you need to\n",
    "complete part 1. **Note that the equations in the slides use\n",
    "the sigmoid activation function and L1 loss, referring to them might make this process easier**. You can also seek and read other\n",
    "sources sure you fully understand how neural networks work. Your TA is also\n",
    "glad to answer any questions.\n",
    "\n",
    "Submit your final answers, along with the steps to calculate them. If you\n",
    "consult external resources for the assignment, please include a link or\n",
    "citation to them too.\n",
    "\n",
    "## Part 2: Training a neural network\n",
    "\n",
    "We're now going to get past the pesky XOR problem that have been the bane of\n",
    "neural networks' existence.\n",
    "\n",
    "First, let's install libraries. We will be using the PyTorch package to train out neural network. You can use\n",
    "`pip` or `conda` to install the `torch` package.\n",
    "\n",
    "Let import the libraries\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f733fe82-8d3a-4bc5-a5c3-1297760922f6",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "import random\n",
    "\n",
    "torch.manual_seed(1)\n",
    "random.seed(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "979fa120-9dc3-4bd7-9027-84996715212b",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Then let's set up the network architecture. We are constructing the same model as the image above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ece5cbd5-f3a1-405e-81ce-c0b6a1cd277c",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "model = torch.nn.Sequential(\n",
    "    # connecting the input to the hidden layer\n",
    "    torch.nn.Linear(2, 2), # takes an vector of 2 numbers and outputs a vector of 2 numbers\n",
    "    torch.nn.Sigmoid(), # activation function\n",
    "    # connecting the hidden layer to the output layer\n",
    "    torch.nn.Linear(2, 1), # takes an vector of 2 numbers and outputs a vector of 1 number\n",
    "    torch.nn.Sigmoid() # activation function\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d87e0108-b9a8-4155-a147-4fe58088633d",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now we need to train the network. Note that we are going to use the binary\n",
    "cross entropy loss function to optimally train this tiny network. This is a\n",
    "loss (error) measure we did not cover in class. We are using it for the\n",
    "assignment because, as its name suggests, it is well-suited for learning binary\n",
    "functions. XOR is of course such a function, having inputs and outputs that are\n",
    "1s and 0s, not continuous quantities.\n",
    "It's best for training if the examples presented to the network are random. So we are going to randomly generate XOR examples in each loop.\n",
    "\n",
    "PyTorch helps us out with automatic differentiation. We just need to use the gradient calculated with the loss function to modify the value of the model parameters (weights).\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9aaf3035-09e1-47c5-8cfe-5877eb2365ac",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "learning_rate = 1\n",
    "def random_binary():\n",
    "    return random.choice([0, 1])\n",
    "\n",
    "loss_fn = torch.nn.BCELoss()\n",
    "for i in range(4000):\n",
    "\n",
    "    # we're creating a random input and output tensor (vector) that follows the\n",
    "    # xor rule\n",
    "    x1, x2 = random_binary(), random_binary()\n",
    "    true_output = torch.Tensor([x1 ^ x2])\n",
    "\n",
    "    # getting the model's predicted output\n",
    "    predicted_output = model(torch.Tensor([x1, x2]))\n",
    "\n",
    "    # calculating the loss (error)\n",
    "    loss = loss_fn(predicted_output, true_output)\n",
    "\n",
    "\n",
    "    # calculating the gradients, in order to update the weights\n",
    "    model.zero_grad()\n",
    "    loss.backward()\n",
    "\n",
    "    with torch.no_grad(): # prevents gradient tracking during updates\n",
    "        # for each parameter (weight and bias) in the model\n",
    "        for param in model.parameters():\n",
    "            # update the parameter using the gradient scaled by the learning\n",
    "            # rate\n",
    "            param -= learning_rate * param.grad\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6803e64-fc25-40e0-96fa-71a171d3f915",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "We can now see what the neural network outputs for a given input. We are asking\n",
    "the model to predict $\\text{XOR}(0, 1)$ and $\\text{XOR}(1, 1)$. For your\n",
    "submission, please print the output for all input combinations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb49b446-6e95-4a12-965a-191c24cfc7d9",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(model(torch.Tensor([0, 1])))\n",
    "print(model(torch.Tensor([1, 1])))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6848cab9-548b-41a1-be86-38728b05b0aa",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "If the neural network is not successful at XOR, try retraining it from scratch or increasing the number of epochs (training iterations)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef38838a-89d1-4eb3-8477-cee01486aab5",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Prying open a neural network\n",
    "Now that we have a trained network, we can look at the weights and biases for each layer of the network. To do so, use "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c2c1663-2016-4e21-835d-6381513b3546",
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "list(model.named_parameters())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bdf066d8-17db-4105-a447-422ed5f7c326",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Here's some sample output \n",
    "\n",
    "```python\n",
    "[('0.weight',\n",
    "  Parameter containing:\n",
    "  tensor([[-6.2839, -6.2317],\n",
    "          [-8.0318, -8.0085]], requires_grad=True)),\n",
    " ('0.bias',\n",
    "  Parameter containing:\n",
    "  tensor([9.3037, 3.4865], requires_grad=True)),\n",
    " ('2.weight',\n",
    "  Parameter containing:\n",
    "  tensor([[ 14.3675, -14.7218]], requires_grad=True)),\n",
    " ('2.bias',\n",
    "  Parameter containing:\n",
    "  tensor([-6.8876], requires_grad=True))]\n",
    "```\n",
    "\n",
    "Note that each of the 2 layers have 2 items in the output. Weights and biases.\n",
    "\n",
    "The weights are represented as a transformation matrix transforming the input\n",
    "to a layer.\n",
    "\n",
    "For example, for the Python output above, the weight connecting\n",
    "$x_1$ to $h_1$ has the value $-6.2839$, and the weight connecting $x_2$\n",
    "to $h_1$ has the value $-6.2317$. The bias value for $h_1$ is $9.3037$, and the bias value for $h_2$ is $3.4865$.\n",
    "\n",
    "## Questions\n",
    "\n",
    "* What were the final weights and biases for your network? How well did it do?\n",
    "* Using the weights and biases for your network, calculate the\n",
    "predictions for each for the four input-output combinations in the XOR\n",
    "truth table (you can find this in the slides or write it down\n",
    "yourself). You may do this manually or using code (looping, making\n",
    "functions, etc.), but do not use the trained model to do so.\n",
    "\n",
    "  To make this easier, here is a function that calculates the sigmoid\n",
    "  of an input vector.\n",
    "\n",
    "  ```python\n",
    "  import numpy as np\n",
    "\n",
    "  def sigmoid(x):\n",
    "    return 1 / (1 + np.exp(-x))\n",
    "  ```\n",
    "\n",
    "* Conduct an analysis of the weights and biases of the network you have just trained. Why does it compute the XOR function? See if you can intepret the neural networks as a combination of simpler logical functions such as OR and AND, or outline a different insight regarding the behavior of the network. Be creative and extensive here!\n",
    "\n",
    "## Credits\n",
    "\n",
    "Part 1 was adapted from University of Wisconsin-Madison's CS 540 class.\n",
    "\n",
    "## Submission guidelines\n",
    "\n",
    "Please submit the `.ipynb` file to Canvas."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "argv": [
    "python",
    "-m",
    "ipykernel_launcher",
    "-f",
    "{connection_file}"
   ],
   "display_name": "Python 3 (ipykernel)",
   "env": null,
   "interrupt_mode": "signal",
   "language": "python",
   "metadata": {
    "debugger": true
   },
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  },
  "name": "assignment-4-neural-networks.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
